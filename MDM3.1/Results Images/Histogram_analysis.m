clear all; close all; clc


im1mean = imread('test1mean.png');
im2mean = imread('test1mean.png');

im1median = imread('test1median.png');
im2median = imread('test2median.png');

%im1wel = imread('Welfords1.png');
%im2wel = imread('Welfords2.png');

im1model1 = imread('test1model1.png');
im2model1 = imread('test2model1.png');


% figure(1); hold on; 
% histogram(im1mean,'facecolor','auto','facealpha',.5,'edgecolor','auto');
% histogram(im1median,'facecolor','auto','facealpha',.5,'edgecolor','auto'); 
% histogram(im1wel,'facecolor','auto','facealpha',.5,'edgecolor','auto');
% legend('Mean', 'Median', 'Welford'); hold off;
% print -dpng Histogramcomparison1

% figure(2); 
% hold on; 
% histogram(im2mean,'facecolor','auto','facealpha',.5,'edgecolor','auto');
% histogram(im2median,'facecolor','auto','facealpha',.5,'edgecolor','auto'); 
% histogram(im2wel,'facecolor','auto','facealpha',.5,'edgecolor','auto');
% legend('Mean', 'Median', 'Welford'); hold off;
%print -dpng Histogramcomparison2

figure();
histogram(im1mean,'facecolor','auto','facealpha',.5,'edgecolor','auto');
histogram(im1median,'facecolor','auto','facealpha',.5,'edgecolor','auto'); 
histogram(im1model1,'facecolor','auto','facealpha',.5,'edgecolor','auto');
%legend('Mean', 'Median','Clipped Variance Model'); 
hold off;

figure(4);
%histogram(im1mean,'facecolor','auto','facealpha',.5,'edgecolor','auto');
histogram(im1median,'facecolor','auto','facealpha',.5,'edgecolor','auto'); 
%histogram(im2model1,'facecolor','auto','facealpha',.5,'edgecolor','auto');
legend('Mean', 'Median','Clipped Variance Model'); hold off;


%figure(2); histogram(im1wel);