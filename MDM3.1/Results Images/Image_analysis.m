%Generating different plots to compare results
clear all; close all; clc


imdata = imread('test1mean.png');

%figure(1); imshow(imdata); title('actual image');

imdata = rgb2gray(imdata);

%figure(2); imshow(imdata); title('gray scale image')


%From YouTube tutorial 'Fast Fourier Transform of an Image in Matlab
%(TUTORIAL) + codes'

F = fft2(uint16(imdata)); 
Fsh = fftshift(F); %Shifting to centered spectrum

A = abs(F);
%figure(3); imshow(A,[]); title('fourier transform')

S = log(1+abs(Fsh));
%figure(4); imshow(uint16(S),[]); %title('Log transformed image');

%print -dpng Fourier_Welford2

figure(5); histogram(imdata);


