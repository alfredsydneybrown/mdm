%Change this here to read the images you want
ImagesM = [];
% for ci=1:51
%     Images{ci} = imread( sprintf('50 Frames/2018-10-24 - 11.47.45 Frame  %02d Trace Displacement Scaled.tiff', 2*ci ) );
%     ImagesM = cat(3, ImagesM, Images{ci});  % Tack on slice i into a new cell.
% end

% for ci=1:31
%     Images{ci} = imread( sprintf('Test Frames 2/2018-10-24 - 11.52.53 Frame  %02d Trace Displacement Scaled.tiff', 2*ci ) );
%     ImagesM = cat(3, ImagesM, Images{ci});  % Tack on slice i into a new cell.
% end

for ci=1:30
    Images{ci} = imread( sprintf('Unflattened Data/set2/noise_v1/noise_v1_set2 Frame  %02d Trace Displacement Scaled.tiff', 2*ci ) );
    ImagesM = cat(3, ImagesM, Images{ci});  % Tack on slice i into a new cell.
end
ImagesM = double(ImagesM);


%Choose three images to initialise our running mean and variance
%Make sure they are within the right range
M = conditional_welford(ImagesM);
imshow(uint16(M))

function [Welford_Im] = conditional_welford(ImagesM)

Im1 = 1;
Im2 = 2;
Im3 = 3;

N = ones(1000,1000);
[M,S] = current_mean_var(ImagesM(:,:,Im1), N,1000); 

N = 2.*ones(1000,1000);
[M,S] = current_mean_var(ImagesM(:,:,Im2), N,1000);

N = 3.*ones(1000,1000);
[M,S] = current_mean_var(ImagesM(:,:,Im3), N,1000);

%Now we condition on other images
i = 1;
N = 3.*ones(1000,1000);
while i <= size(ImagesM,3)
    if (i ~= Im1) && (i ~= Im2) && (i ~= Im3)
    Im = ImagesM(:,:,i);
    Im(Im > M + sqrt(S) | Im < M - sqrt(S)) = 0;
    %Im(Im == 0) = 1;
    Mask1 = Im;
    Mask1(Mask1 ~= 0) = 1;
    N = N + Mask1;
    [M,S] = current_mean_var(Im,N,1000);
    end
    i = i+1;
end

Welford_Im = M;
end

%Welford_Im = M;
%figure(1);
%imshow(uint16(mean(ImagesM,3)));

%figure(2);
%imshow(uint16(M));

%corrcoef(M,mean(ImagesM,3));

%figure(3);
%imshow(uint16(ImagesM(:,:,3)))

%Returns the current mean and variance 
function [M,S] = current_mean_var(Im2D, N, dim)

global M
global S

if N == ones(dim,dim)
    M = Im2D;
    S = zeros(dim,dim);
else
    if ismember(0,Im2D)
        Mask = Im2D;
        Mask(Mask ~= 0) = 1;
        Mnext = M + (Im2D - M.*Mask)./N;
        S = S + (Im2D - M.*Mask).*(Im2D - Mnext.*Mask);
        M = Mnext;
    else
        Mnext = M + (Im2D - M)./N;
        S = S + (Im2D - M).*(Im2D - Mnext);
        M = Mnext;
    end
end
end


%%Ignore the following:
%sum(sum(uint16(mean(Im3,3)) - uint16(M)))

% A = [1 1; 2 2];
% 
% N = ones(2,2);
% 
% [M,S] = current_mean_var(A,N,2)
% 
% B = [3 3; 5 5];
% 
% N = N + ones(2,2)
% 
% [M,S] = current_mean_var(B,N,2)
% 
% C = [3 0; 5 0]
% 
% N = N + [1 0; 1 0]
% 
% [M,S] = current_mean_var(C,N,2)
% 
% mean(cat(3,A,B),3)


%Im3 = cat(3,ImagesM(:,:,1),ImagesM(:,:,2),ImagesM(:,:,3));


% i = 1;
% N = ones(1000,1000);
% while i <= 3
%     [M,S] = current_mean_var(ImagesM(:,:,i), N,1000);
%     N = N + ones(1000,1000);
%     i = i + 1;
% end