pictureZ = [];

for x=1:1000
    for y=1:1000
        
        Zvalues = [];
        c2 = 1;
        MeanHistory = [];
        MeanChange = [];
        
        Zmean = mean(double(A1(x,y,:)));
        Zvar = var(double(A1(x,y,:)));
        upperZ = Zmean + 0.2*Zvar;
        lowerZ = Zmean - 0.2*Zvar;
        
        for z=1:51
            if (A1(x,y,z) < upperZ) && (A1(x,y,z) > lowerZ)
                Zvalues(z) =  A1(x,y,z);
                MeanHistory(c2) = Zmean;
                Zmean = mean(Zvalues);
                
                c2 = c2 +1;
                if c2 == 6
                    c2 = 1;
                end
                MeanChange = MeanHistory - Zmean;
                if (mean(MeanChange) < 0.1*Zvar) && (z > 5) 
                    break
                end 
            end
        end    
       
       pictureZ(x,y) = mean(Zvalues);
        
    end 
end

imshow(uint16(pictureZ))