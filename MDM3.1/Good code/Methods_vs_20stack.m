%This code here is there to evaluate visually the differences in quality
%of the Conditional Welford Online Algorithm compared to the mean and
%median for each of the sets of data, for the first 20 images collected 
%as this is the maximum number of stacks that the AFM team wishes to use.

ImagesM1 = [];
ImagesM2 = [];
ImagesM3 = [];
ImagesM4 = [];
ImagesM5 = [];
ImagesM6 = [];
ImagesM7 = [];
ImagesM8 = [];
ImagesM9 = [];
ImagesM10 = [];
% 
% for ci=1:53
%     Images{ci} = imread( sprintf('Unflattened Data/set1/no_added_noise_set1 Frame  %02d Trace Displacement Scaled.tiff', 2*ci ) );
%     ImagesM1 = cat(3, ImagesM1, Images{ci});  % Tack on slice i into a new cell.
% end

% for ci=1:58
%     Images{ci} = imread( sprintf('Unflattened Data/set2/no_noise/no_added_noise_set2 Frame  %02d Trace Displacement Scaled.tiff', 2*ci ) );
%     ImagesM2 = cat(3, ImagesM2, Images{ci});  % Tack on slice i into a new cell.
% end

% for ci=1:30
%     Images{ci} = imread( sprintf('Unflattened Data/set2/noise_v1/noise_v1_set2 Frame  %02d Trace Displacement Scaled.tiff', 2*ci ) );
%     ImagesM3 = cat(3, ImagesM3, Images{ci});  % Tack on slice i into a new cell.
% end

% for ci=1:49
%     Images{ci} = imread( sprintf('Unflattened Data/set2/noise_v2/noise_v2_set2 Frame  %02d Trace Displacement Scaled.tiff', 2*ci ) );
%     ImagesM4 = cat(3, ImagesM4, Images{ci});  % Tack on slice i into a new cell.
% end
% 
% for ci=1:31
%     Images{ci} = imread( sprintf('Unflattened Data/set2/noise_v3/noise_v3_set2 Frame  %02d Trace Displacement Scaled.tiff', 2*ci ) );
%     ImagesM5 = cat(3, ImagesM5, Images{ci});  % Tack on slice i into a new cell.
% end

% for ci=1:49
%     Images{ci} = imread( sprintf('Unflattened Data/set2/noise_v4/noise_v4_set2 Frame  %02d Trace Displacement Scaled.tiff', 2*ci ) );
%     ImagesM6 = cat(3, ImagesM6, Images{ci});  % Tack on slice i into a new cell.
% end
% 
% for ci=1:50
%     Images{ci} = imread( sprintf('Unflattened Data/set3/no_added_noise_set3 Frame  %02d Trace Displacement Scaled.tiff', 2*ci ) );
%     ImagesM7 = cat(3, ImagesM7, Images{ci});  % Tack on slice i into a new cell.
% end

% for ci=1:51
%     Images{ci} = imread( sprintf('Unflattened Data/set4/2018-10-24 - 11.47.45 Frame  %02d Trace Displacement Scaled.tiff', 2*ci ) );
%     ImagesM8 = cat(3, ImagesM8, Images{ci});  % Tack on slice i into a new cell.
% end

for ci=1:53
    Images{ci} = imread( sprintf('Unflattened Data/set5/2018-10-24 - 11.42.36 Frame  %02d Trace Displacement Scaled.tiff', 2*ci ) );
    ImagesM9 = cat(3, ImagesM9, Images{ci});  % Tack on slice i into a new cell.
end
% 
% for ci=1:31
%     Images{ci} = imread(sprintf('Unflattened Data/set6/2018-10-24 - 11.52.53 Frame  %02d Trace Displacement Scaled.tiff', 2*ci));
%     ImagesM10 = cat(3, ImagesM10, Images{ci});  % Tack on slice i into a new cell.
% end

M1 = double(ImagesM1);
M2 = double(ImagesM2);
M3 = double(ImagesM3);
M4 = double(ImagesM4);
M5 = double(ImagesM5);
M6 = double(ImagesM6);
M7 = double(ImagesM7);
M8 = double(ImagesM8);
M9 = double(ImagesM9);
M10 = double(ImagesM10);


% subplot(3,3,1);
% imshow(uint16(mean(M1,3)));
% title('set 1')
% 
% subplot(3,3,2);
% imshow(uint16(mean(M2,3)));
% title('set 2')
% 
% subplot(3,3,3);
% imshow(uint16(mean(M3,3)));
% title('set 3')
% 
% subplot(3,3,4);
% imshow(uint16(mean(M4,3)));
% title('set 4')
% 
% subplot(3,3,5);
% imshow(uint16(mean(M5,3)));
% title('set 5')
% 
% subplot(3,3,6);
% imshow(uint16(mean(M6,3)));
% title('set 6')
% 
% subplot(3,3,7);
% imshow(uint16(mean(M7,3)));
% title('set 7')
% 
% subplot(3,3,8);
% imshow(uint16(mean(M8,3)));
% title('set 8')
% 
% subplot(3,3,9);
% imshow(uint16(mean(M9,3)));
% title('set 9')
% 
% figure(2); imshow(uint16(mean(M10,3)));
% title('set 10')

Stacks_considered = 30;%note, can't be more than data
Error_welford = zeros(1,Stacks_considered);
Error_mean = zeros(1,Stacks_considered);
Error_median = zeros(1,Stacks_considered);

MeanIm = mean(M9(:,:,1:Stacks_considered),3);
MedianIm = median(M9(:,:,1:Stacks_considered),3);

for i = 1:Stacks_considered
    WelIm = conditional_welford(M9(:,:,1:i));
    Error_welford(i) = sum(sum(abs(WelIm - MeanIm)));
    Error_mean(i) = sum(sum(abs(mean(M9(:,:,1:i),3) - MeanIm)));
    Error_median(i) = sum(sum(abs(median(M9(:,:,1:i),3) - MeanIm)));
end


hold on; 
figure(1);
h = gcf;
plot(1:size(Error_welford,2), Error_welford,'r', 'linewidth',2); 
plot(1:size(Error_mean,2), Error_mean, 'b', 'linewidth', 2); 
plot(1:size(Error_median,2), Error_median, 'g', 'linewidth',2);
%lgd = legend({['Conditional Online' newline 'Welford Algorithm'], 'Mean', 'Median'});
lgd = legend('Welford', 'Mean', 'Median');
lgd.FontSize = 14;
lgd.Interpreter = 'latex';
xl = xlabel('Aggregate Frames','Interpreter','latex');
xl.FontSize = 14;
yl = ylabel('Summed Difference to Mean of 30 Frames','Interpreter','latex');
yl.FontSize = 14;
set(gcf,'color','w');
pl = line([20 20],[10^8 4*10^8]);
pl.Color = 'black';
pl.LineStyle = '--';
t = text(20.4,2.9*10^8, ['Ideal Number' newline 'of Frames'],'Fontsize',14);
t.Interpreter = 'latex';
%saveas(figure(1),[pwd '/Results/set9_error_from_mean.png']);
hold off;




