function [ImagesM, Images, xBar, Median] = ReadTiffs()

ImagesM = [];
for ci=1:51
    Images{ci} = imread( sprintf('50 Frames/2018-10-24 - 11.47.45 Frame  %02d Trace Displacement Scaled.tiff', 2*ci ) );
    ImagesM = cat(3, ImagesM, Images{ci});  % Tack on slice i into a new cell.
end

xBar = mean(ImagesM,3);
Median = median(ImagesM, 3);


%Display
%imshow(uint16(xBar))
%imshow(uint16(Median))

end
