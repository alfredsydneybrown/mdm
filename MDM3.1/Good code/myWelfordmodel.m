% ImagesM = [];
% for ci=1:51
%     Images{ci} = imread( sprintf('50 Frames/2018-10-24 - 11.47.45 Frame  %02d Trace Displacement Scaled.tiff', 2*ci ) );
%     ImagesM = cat(3, ImagesM, Images{ci});  % Tack on slice i into a new cell.
% end

%inspired by https://www.embeddedrelated.com/showarticle/785.php
Im3D = double(ImagesM);

i = 1;
Sum = double(zeros(size(Im3D(:,:,1))));
Sumofsquares = Sum;
n = 0;

while i <= size(Im3D,3)
    NewIm = Im3D(:,:,i);
    Sum = Sum + NewIm;
    Sumofsquares = Sumofsquares + NewIm.^2;
    n = n + 1;
    Newmean = Sum./n;
    i = i + 1;
end


k = 0;
Sum = double(zeros(size(Im3D(:,:,1))));

for i = 1:size(Im3D,3)
    k = k + 1;
    if k == 1
        M = Im3D(:,:,i);
        S = zeros(1000,1000);
    else 
        
        Mnext = M + (Im3D(:,:,i) - M)./k;
        S = S + (Im3D(:,:,i) - M).*(Im3D(:,:,i) - Mnext);
        M = Mnext;
    end
    NewIm = Im3D(:,:,i);
    Sum = Sum + NewIm;
end

disp(size(Newmean))
Actualmean = uint16(mean(Im3D,3));
Runningmean1 = uint16(Newmean);
RunningWelford = uint16(M);
%figure(1); imshow(Actualmean);
%figure(2); imshow(Runningmean);
disp(sum(sum(Actualmean - RunningWelford)))
disp(sum(sum(uint16(var(Im3D,0,3)) - uint16(S))))