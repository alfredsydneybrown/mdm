pictureZ = [];

for x=1:1000
    for y=1:1000
        Zvalues = [];
        Zmean = 0; 
        Zvar = 0;
        start = [];
        
        start(1) = A1(x,y,1);
        start(2) = A1(x,y,24);
        start(3) = A1(x,y,51);
        Zmean = mean(start);
        Zvar = var(start);
        upperZ = Zmean + 0.5*Zvar;
        lowerZ = Zmean - 0.5*Zvar;
        
       
       for z = 2:50
           if z ~=24
               if (A1(x,y,z) < upperZ) && (A1(x,y,z) > lowerZ)
                    Zvalues(z) =  A1(x,y,z);
                    Zmean = mean(Zvalues);
                    Zvar = var(Zvalues);
                    upperZ = Zmean + 0.5*Zvar;
                    lowerZ = Zmean - 0.5*Zvar;
               end 
           end
       end 
       
       pictureZ(x,y) = Zmean;
        
    end 
end

imshow(uint16(pictureZ))