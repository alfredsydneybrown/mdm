function [ImagesM, Images, xBar, Median] = ReadTiffs()

ImagesM = [];
for ci=1:31
    Images{ci} = imread( sprintf('2018-10-24 - 11.52.53 Frame  %02d Trace Displacement Scaled.tiff', 2*ci ) );
    ImagesM = cat(3, ImagesM, Images{ci});  % Tack on slice i into a new cell.
end

xBar = mean(ImagesM,3);
Median = median(ImagesM, 3);


%Display
figure();
imshow(uint16(xBar));
print('test_2_mean','-dpng');
figure();
imshow(uint16(Median));
print('test_2_median','-dpng');


end
