clear

  [ImagesM, Images, xBar, Median] = ReadTiffs();
  
  %ImagesM = im2double(ImagesM);
  
  N = 31; % Number of iterations [-]
  w = 31; % Window size [-]
  
  x_hist = ImagesM(:,:,1);  % Initialize history with first data frame
  m = uint16(mean(x_hist,3)); % Initialize mean
  s2 = uint16(zeros(size(x_hist))); % Initialize variance
  
%   If the number of frames in x_hist equals 1, m_prev and s2_prev need to be initialized to x_hist and 
%   zeros(size(x_hist)) respectively.  
  
  m_prev = x_hist;
  s2_prev = s2; %zeros(size(x_hist));
  
  for i = 2:N % NOTE: you have to start at 2, otherwise the variance will be incorrect while i<w
    
      % Get new frame
      x_new = ImagesM(:,:,i);
    
      % Compute moving-window mean (m) and moving-window variance (s2)
      [m, s2, x_hist] = moving_mean_var(m, s2, x_hist, x_new, w);
      
  end
  figure();
  imshow(uint16(m));
  print('Welfords2','-dpng')