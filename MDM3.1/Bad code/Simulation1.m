%Sampling erros
%Random voices
%Dust
%Angular shift
%Drift
%Random error

a = 10;
n = 100;

TrueSample = randi(a,n,n);


%Body

NoiseMatrix = RandomNoise(TrueSample,n);
NoiseList = path(NoiseMatrix);


while (i <= length(NoiseList)) && (i > 1)
    [DriftPath, i] = Drift(NoiseList,i);
    i = i + 1;
end

%functions
function [PathList] = path(SampleMatrix)
   PathList = SampleMatrix(:);      
end


function [NoiseMatrix] = RandomNoise(SampleMatrix,n) 
    NoiseMatrix = SampleMatrix + randn(n,n); 
end

function [DriftList, NewIndex] = Drift(PathList, Index)
    deltaZ = abs(PathList(Index-1) - PathList(Index));
    MaxdeltaZ = max(PathList) - min(PathList);
    ProbdeltaZ = (1/MaxdeltaZ)*deltaZ;
    %We need different decay rates depending on the sign of deltaZ
    Number_of_i = round(ProbdeltaZ * 5);
    j = 1 + Index;
    while (PathList(j) < PathList(Index)) && (j <= length(PathList))
        PathList(j) = PathList(Index);
        j = j+1;
    end
    NewIndex = j;
    DriftList = PathList;
end
