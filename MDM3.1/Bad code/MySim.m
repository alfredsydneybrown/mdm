%My simulation

a = 10;
n = 100;

TrueSample = randi(a,n,n);
NoiseList = path(RandomNoise(TrueSample,n));

EmptyList = zeros(1,length(NoiseList))

%functions
function [PathList] = path(SampleMatrix)
   PathList = SampleMatrix(:);      
end

function [NoiseMatrix] = RandomNoise(SampleMatrix,n) 
    NoiseMatrix = SampleMatrix + randn(n,n); 
end

function [Z,I] = drift(List,Index)
    Z = abs(List(Index + 1) - List(Index));
    MaxdeltaZ = abs(max(List) - min(List));
    ProbdeltaZ = (1/MaxdeltaZ)*Z;
    I = round(ProbdeltaZ * 10);  
end
